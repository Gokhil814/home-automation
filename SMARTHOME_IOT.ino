// DHT Temperature & Humidity Sensor
// Unified Sensor Library Example
// Written by Tony DiCola for Adafruit Industries
// Released under an MIT license.

// Depends on the following Arduino libraries:
// - Adafruit Unified Sensor Library: https://github.com/adafruit/Adafruit_Sensor
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

#define DHTPIN            12         // Pin which is connected to the DHT sensor.
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
String rxtext="";
int flag=0;
#define DEBUG 1 
const int relay1=22;
const int relay2=24;
const int relay3=26;
const int relay4=28;
const int reed =3;
const int pir= 2;
const int mq13 = 11;
#define relay1_on digitalWrite(relay1,HIGH);
#define relay1_off digitalWrite(relay1,LOW);

#define relay2_on digitalWrite(relay2,HIGH);
#define relay2_off digitalWrite(relay2,LOW);

#define relay3_on digitalWrite(relay3,HIGH);
#define relay3_off digitalWrite(relay3,LOW);

#define relay4_on digitalWrite(relay4,HIGH);
#define relay4_off digitalWrite(relay4,LOW);

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2
// Uncomment the type of sensor in use:
//#define DHTTYPE           DHT11     // DHT 11 
//#define DHTTYPE           DHT22     // DHT 22 (AM2302)
#define DHTTYPE           DHT21     // DHT 21 (AM2301)


#define LOGO16_GLCD_HEIGHT 16 
#define LOGO16_GLCD_WIDTH  16 
static const unsigned char PROGMEM logo16_glcd_bmp[] =
{ B00000000, B11000000,
  B00000001, B11000000,
  B00000001, B11000000,
  B00000011, B11100000,
  B11110011, B11100000,
  B11111110, B11111000,
  B01111110, B11111111,
  B00110011, B10011111,
  B00011111, B11111100,
  B00001101, B01110000,
  B00011011, B10100000,
  B00111111, B11100000,
  B00111111, B11110000,
  B01111100, B11110000,
  B01110000, B01110000,
  B00000000, B00110000 };

#if (SSD1306_LCDHEIGHT != 32)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

DHT_Unified dht(DHTPIN, DHTTYPE);

uint32_t delayMS;

int REED, PIR, MQ13;
void setup() {
  Serial.begin(9600); 
  pinMode(relay1,OUTPUT);
  pinMode(relay4,OUTPUT);
  pinMode(relay3,OUTPUT);
  pinMode(relay2,OUTPUT);
  pinMode(reed, INPUT_PULLUP);
  pinMode(pir, INPUT);
  pinMode(mq13,INPUT);
  relay1_off;relay2_off;relay3_off;relay4_off;
  // Initialize device.
  dht.begin();
  Serial.println("DHTxx Unified Sensor Example");
  // Print temperature sensor details.
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.println("Temperature");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" *C");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" *C");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" *C");  
  Serial.println("------------------------------------");
  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.println("Humidity");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println("%");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println("%");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println("%");  
  Serial.println("------------------------------------");
  // Set delay between sensor readings based on sensor details.
  delayMS = sensor.min_delay / 1000;
  
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
   display.display();
  delay(2000);

  // Clear the buffer.
  display.clearDisplay();
    testscrolltext();
  delay(2000);
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("   SMART");
  display.println("   HOME");
  display.display();
  delay(3000);
  Serial3.begin(115200);
  Serial3.println("AT");
  delay(1000);
  Serial.begin(9600);
  Serial.print("OK");
  sendData("AT+RST\r\n",2000,DEBUG); // reset module
  delay(2000);
  sendData("AT+GMR\r\n",2000,DEBUG); // reset module
  delay(2000);
  sendData("AT+CWLAP\r\n",1000,DEBUG);
  delay(2000);delay(2000);
  sendData("AT+CWMODE=3\r\n",1000,DEBUG); // configure as access point
  delay(100); delay(1000);
  sendData("AT+CWJAP=\"sude\",\"sude1234\"\r\n",1000,DEBUG);
  //sendData("AT+CWJAP=\"kgk\",\"123456abc\"\r\n",1000,DEBUG);
  
   delay(10000);
 
  sendData("AT+CIFSR\r\n",1000,DEBUG); // get ip address
  delay(5000);
 sendData("AT+CIFSR\r\n",1000,DEBUG); // get ip address
 delay(1000);
 delay(1000);
  delay(1000);
}
int temperature, humidity;
void loop() {
    delay(delayMS);
  sensors_event_t event;  
  dht.temperature().getEvent(&event);
  if (isnan(event.temperature)) {
    Serial.println("Error reading temperature!");
  }
  else
  {
    Serial.print("Temperature: ");
    Serial.print(event.temperature);
    Serial.println(" *C");
    temperature=event.temperature;
  }
  // Get humidity event and print its value.
  dht.humidity().getEvent(&event);
  if (isnan(event.relative_humidity)) {
    Serial.println("Error reading humidity!");
  }
  else {
    Serial.print("Humidity: ");
    Serial.print(event.relative_humidity);
    Serial.println("%");
    humidity=event.relative_humidity;
  }
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("TEMP:");
  display.print(temperature);
  display.write(0xF8);
   display.print("C");
  display.display();
  delay(500);
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("HUMIDITY:");
  display.print(humidity);
  display.print("%");
  display.display();
  delay(500);
  PIR=digitalRead(pir);
  REED= digitalRead(reed);
  MQ13=digitalRead(mq13);

  if(PIR==1)
  {
    display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("PERSON ");
  display.print("DETECTED");
  display.display();
  delay(500);
  }
  if(REED==1)
  {
    display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("DOOR ");
  display.print("OPEN");
  display.display();
  delay(500);
   }
  else
  {
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("DOOR ");
  display.print("CLOSED");
  display.display();
  delay(500);
   }
    if(MQ13==0)
  {
    display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("  GAS");
  display.print("LEAKAGE!!");
  display.display();
  delay(500);
   }
   /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

     String cmd = "AT+CIPSTART=\"TCP\",\"";
 
   cmd += "192.168.43.36";
  cmd += "\",80";
 Serial3.println(cmd);

 if( Serial3.find("Error")){
    Serial.println("AT+CIPSTART error");
    return;
  }
    String getStr = "GET /homei/sensors.php?api=Cybq3leHhq9HFFL&pass=26a52726a221c26f90642377a40f82aa&s1=";
  getStr += String(temperature);
  getStr +="&s2=";
  getStr += String(humidity);
    getStr +="&s3=";
  getStr += String(REED);
    getStr +="&s4=";
  getStr += String(MQ13);
    getStr +="&s5=";
  getStr += String(PIR);
  getStr += "\r\n\r\n";
 Serial.print(getStr);
  // send data length
  cmd = "AT+CIPSEND=";
  cmd += String(getStr.length());
   Serial3.println(cmd);
   delay(200);


delay(500);

if(Serial3.find(">")){
    Serial3.print(getStr);
     Serial.println("OK");
  }
  else{
     Serial3.println("AT+CIPCLOSE");
    // alert user
    Serial.println("AT+CIPCLOSE");
  }
  delay(500);
//delay(500);delay(500);
Serial3.println("AT+CIPCLOSE");
delay(500);
///////////////////////////////////////////////

 String cmd1 = "AT+CIPSTART=\"TCP\",\"";
 
   cmd1 += "192.168.43.36";
  cmd1 += "\",80";
 Serial3.println(cmd1);

 if( Serial3.find("Error")){
    Serial.println("AT+CIPSTART error");
    return;
  }
  
 String getStr1 = "GET /homei/swstatus.php?api=Cybq3leHhq9HFFL&pass=26a52726a221c26f90642377a40f82aa";
  getStr1 += "\r\n\r\n";
  cmd = "AT+CIPSEND=";
  cmd += String(getStr1.length());
   Serial3.println(cmd);
   delay(200);
   sendData(getStr1,1000,DEBUG);
 //Serial1.print(getStr1);
 Serial3.print(getStr1);
 delay(500);
Serial3.println("AT+CIPCLOSE");

delay(500);
/////////////////////////////////////////////////////////////////////////////
}

void testscrolltext(void) {
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(10,0);
  display.clearDisplay();
  display.println("KELTRON");
  display.display();
  delay(1);
 
  display.startscrollright(0x00, 0x0F);
  delay(2000);
  display.stopscroll();
  delay(1000);
  display.startscrollleft(0x00, 0x0F);
  delay(2000);
  display.stopscroll();
  delay(1000);    
  display.startscrolldiagright(0x00, 0x07);
  delay(2000);
  display.startscrolldiagleft(0x00, 0x07);
  delay(2000);
  display.stopscroll();
}

 String sendData(String command, const int timeout, boolean debug)
{
    String response = "";
    
     Serial3.print(command); // send the read character to the esp8266
    
    long int time = millis();
    
    while( (time+timeout) > millis())
    {
      while( Serial3.available())
      {
        
        // The esp has data so display its output to the serial window 
        char c =  Serial3.read(); // read the next character.
        response+=c;
        if(c=='$'){flag=1;}
      if(flag==1){rxtext+=c;}  
      if(c=='@'){ 
            flag=0;Serial.print("rxtext=");
            Serial.print(rxtext);
            if(rxtext[1]=='O'){relay1_on;}if(rxtext[1]=='F'){relay1_off;}
            if(rxtext[3]=='O'){relay2_on;}if(rxtext[3]=='F'){relay2_off;}
            if(rxtext[5]=='O'){relay3_on;}if(rxtext[5]=='F'){relay3_off;}
            if(rxtext[7]=='O'){relay4_on;}if(rxtext[7]=='F'){relay4_off;}
            rxtext="";
           }
//      }  
    }
    }
    if(debug)
    {
      Serial.print(response);
    }
    
    return response;
}

